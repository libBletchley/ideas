---
category: Your category
title: Your custom Title
short: A simple and short description
author: Your codeberg name 
contributors: [first, second, codeberg users that helps to improve this idea, optional if nobody helps, yet]  
---

Your full description.

Do here what you want.

## Additional notes
- A note helpful for people that wanna develope this idea to an project
- Or a optional feature that would nice-to-have
- A project that does these stuff, but bad and not the thing I want.

## Projects by this idea
- A project inspired by this idea
- [Test Project](https://codeberg.org/fossdd/ideas)