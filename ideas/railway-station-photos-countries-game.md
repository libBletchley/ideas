---
category: Game
title: Railway Station Photos Countries Game
short: A brain-train game around the Railway Station Photos project
author: mondstern
---

There are always 2 runs. In the first run, you see xx station photos from Germany, which are only shown for a very short time. In the second run, many more are shown and you have to click on which ones you have seen. 

xx The number can be adjusted to train your memory.

The whole thing can then also be applied to the other [countries](https://map.railway-stations.org/)

Variations:

The images are displayed for 0.5 seconds at a time.
The images are displayed for 1 second at a time.
The images are displayed for 2 seconds at a time,
The images are displayed for 3 seconds at a time,

So something like easy, medium, hard and very hard.

## Additional notes
- App on [F-Droid](https://f-droid.org/packages/de.bahnhoefe.deutschlands.bahnhofsfotos/)
- Sourcecode on [Github](https://github.com/RailwayStations)
