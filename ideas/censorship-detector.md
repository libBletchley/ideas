---
category: Security
title: Censorship detector-whistleblower
short: a tool to detect Reddit shadowbanning and Lemmy censorship
author: libBletchley
---

Maintains local copy of posts to Reddit & Lemmy.  Periodically checks
public (cookieless) view of those articles.  Logs & alerts on
shadowban/censorship/moderator actions.

Possible features:

* Compose/edit posts in emacs.
* Locally stored copy gets robotically posted to Reddit subs, Lemmy
  communities, relevant phpBB forums, and usenet.
* Collaborate on metrics to expose patterns of censorship by forum,
  moderator, or ownership.
* Automatically post copies of censored material in a out-of-band
  place like usenet, or venue with different people in power,
  designated as a consorship observatory.
