---
category: Android
title: ADB Webcam
short: You can use your smartphone camera as a webcam via ADB
author: mondstern
---

Android App that transmit your camera over the [Android Debug Bridge](https://developer.android.com/studio/command-line/adb) to your computer as a webcam