---
category: Android
title: Watermark App
short: Simple app that watermarks images
author: mondstern
---

If you could watermark one image and if you could watermark several images at once, that would be fantastic.

## Additional notes
- In F-Droid there is only one watermark app and that is only in Chinese. So one in German or in English, which can preferably be translated to weblate.
