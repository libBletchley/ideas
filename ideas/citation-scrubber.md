---
category: Network Neutrality
title: Citation scrubber/optimizer
short: takes a blog article and inspects every URL for privacy abuses-- proposes alternates
author: libBletchley
---

# Problem:

Say you write blogs and the like.  You cite URLs all over the place,
and you'll be damned if you have time to check whether the URLs lead
to sites that are Tor hostile or on CloudFlare.  Around 30% of all
URLs lead to an exclusive walled-garden where CloudFlare abuses their
power to discriminate against Tor users.  This means your article is
conducive to access inequality -- not good from a network neutrality
point of view.

# Solution

Before publishing, you run your article through a tool that looks for
links that lead to assets that are exclusive-- links that marginalize
a segment of the public.  The tool:

1) Accepts text-based file or a link to an already published doc, parses out all URLs and checks for:
   - Tor hostility (403, CloudFlare, Impurva, tar-pitting)
   - assets of tech giants (CloudFlare, Amazon AWS, Google Cloud, MS Azure, etc)
   - JavaScript that fails LibreJS
   - dead links
   
2) Creates report showing:
   - any offending discoveries from the above checks
   - most recent mirror link found in wayback machine (regardless of article quality/ethics, in case the article later disappears)
   - alternate links to nefarious/harmful links

3) Produces new markdown version (if the input was in markdown),
   which automatically applies the suggested changes.  Perhaps give
   an option to retain the bad links but annotate warnings.
