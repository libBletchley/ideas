---
category: Console
title: Fastlane structures Generator
short: Generate fastlane structures as a CLI
author: fossdd
---

I'm talking about the fastlane structures we use at F-Droid for metadata like descriptions. Here is a cheat sheet: https://gitlab.com/snippets/1895688

Most developers doesn't know how to create them.

Maybe create a simple CLI tool that asks Summary, Descriptions,.. and does that in the right folders.

## Additional notes
- I read something about [`yo`](https://yeoman.io/). Not sure if this is is a good environment for this idea.