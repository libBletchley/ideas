---
category: Communications
title: Wire-Bitlbee plugin
short: make it possible to use Wire over an IRC app instead of the bloated native app
author: libBletchley
---

Wire is a better alternative to Signal, but the bloated client app is
Electron based. Electron really is absurdly fat for what it does.  A
bitlbee plugin would make it possible to use any IRC client the user
wants.  This wouldn't only avoid the bloat, but it's extremely
convenient for those who already use IRC and bitlbee to aggregate
their Mastodon chatter in one place.  Each Wire contact could have a
separate IRC channel/window.  So it would also enable us to avoid the
GUI, and make Wire accessible to terminal users.

It could also make it more convenient to manage a multiple identities.
You wouldn't put 12+ morbidly obese heavyweight people in a Volkswagon
Bug, would you?  No.  So you also wouldn't run 12+ Electron-based apps
on a home PC with ~2-4gb RAM either.  A bitlbee could be made to
handle 12+ accounts no problem.
