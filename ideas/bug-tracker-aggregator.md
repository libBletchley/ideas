---
category: Network Neutrality
title: Free-world-compatible bug tracker aggregator
short: give people ways to submit bug reports without using walled-gardens like Github and Gitlab.com
author: libBletchley
---

(problem) You intend to report a bug, but the project officially
          uses an exceptionally controversial bug tracker
          (e.g. Gitlab.com or MS Github).  This
          [discourages](https://infosec.exchange/@bojkotiMalbona/104637098084869887)
          bug reports.

(solution) Users report the bug wherever they want (forum, mailing
           list, Mastodon, usenet), then a reference to the ad-hoc bug
           report is indexed somewhere so users can browse all
           existing reports for a particular project.  A similar idea
           is proposed
           [here](https://pleroma.libretux.com/objects/c2e53ffd-212b-42c7-92cf-2ab7422e0372).
