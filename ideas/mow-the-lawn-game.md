---
category: Game
title: Mow the Lawn Game
short: A android FOSS game where you mow the lawn
author: mondstern
---

You mow the lawn with your lawnmower and the higher you get in the level, the more complicated it becomes for you to finish because more and more things come up that disturb you:
Sandpit in the middle / molehills sprouting from the ground / rubbish lying in the grass / a tree / a fallen branch from a tree / bushes / a river in the garden / a narrow footbridge separating one lawn from the other, etc.

## Additional notes
- Similar to [Leaf Blower Revolution](https://store.steampowered.com/app/1468260/Leaf_Blower_Revolution__Idle_Game/).
