---
category: Network Neutrality
title: Email address free-society-compatibility tester
short: before sending an email, you need to know the ramifications for a particular address
author: libBletchley
---

# Problem:

Email is broken.  Google and Microsoft have ruined email by forcing
all senders to relay through a third-party.  They reject email if they
don't like the IP address of the sending server.  Many smaller
providers have started to follow their lead.  Some will even reject a
message if the domain portion of the FROM field doesn't match the
reverse IP lookup of the sending SMTP server.  Even fsf.org has
started doing that in the past year.  These attacks on network
neutrality are winning.  The tech giants are winning.  The free world
is losing.

# Solution:

We need a tool that performs a number of checks on an email address so
that you don't waste time writing an email that will be refused.

The tool does an MX lookup & checks whether an email address is hosted
in a walled-garden like Gmail or MS Outlook, and reports known
situations that the server refuses RFC-compliant messages. E.g. some
servers will reject a message if the domain of the FROM address
doesn't match the reverse lookup of the connecting IP; some reject
connections from dynamic IPs, thus forcing senders to share the
message with another third party.

Perhaps state the retention policy of the server, if known, and/or
the legal retention limits in that jurisdiction.

Checks whether the email address has a PGP key on public keyrings.

## Advanced features for future versions:

* If your email bounces, check the IP address of your postfix server
  to see which blacklists you are on.  Log those lists along with the
  MX server of the recipient.
  
* Share and compare your failure data with others to crowdsource and
  expose the netneutrality-disrespecting email service providers, and
  pin down which blacklist their using to feed into better
  predictions.
