---
category: Game
title: F-Droid Game
short: A F-Droid apps icons guessing game
author: mondstern
---

shows you an app logo from the F-Droid app store
and several suggestions, e.g. four, on which you then have to click to find out which is the right logo.
the possibility to click on it is also only short and if you click on it wrong or not so fast, the next one is shown.

in the settings you can choose 
* how many app logos are displayed (e.g. 20, 50 or whatever). 
* in which time you want to play / train
* how short the display of the app logos is
* whether a green thumbs up is shown for each correct score and a red thumbs down is shown for an incorrect score.

variants:
* only one icon is shown briefly, it then fades out and the suggestions are then displayed.
* the other way round: there is an F-Droid app and you have to choose the right one from 4 icons.
