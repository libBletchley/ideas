#!/usr/bin/env python3
"""
Generate the files in this Repository
"""

import os

README = "README.md"
CONTRIBUTORS = "CONTRIBUTORS.md"
CODEOWNERS = "CODEOWNERS"
EXCLUDED_FILES = [
    "CONTRIBUTING.md",
    "LICENSE",
    "readme.py",
    "TEMPLATE.md",
    CONTRIBUTORS,
    README,
    CODEOWNERS,
]
SUBDIR = "ideas"

categories = {}
contributors = {}
authors = {}

for file in os.listdir(SUBDIR):
    if not file.endswith(".md"):
        continue
    if file in EXCLUDED_FILES:
        continue
    idea = {}
    idea["file"] = SUBDIR + "/" + file
    with open(idea["file"]) as f:
        lines = f.read().splitlines(True)
        for line in lines:
            if line.startswith("category: "):
                idea["category"] = line.replace("category: ", "").replace("\n", "")
            if line.startswith("title: "):
                idea["title"] = line.replace("title: ", "").replace("\n", "")
            if line.startswith("short: "):
                idea["short"] = line.replace("short: ", "").replace("\n", "")
            if line.startswith("contributors: "):
                for contributor in (
                    line.replace("contributors: ", "")
                    .replace("[", "")
                    .replace("]", "")
                    .replace("\n", "")
                    .split(", ")
                ):
                    if contributor not in contributors:
                        contributors[contributor] = []
                    contributors[contributor].append(idea)
            if line.startswith("author: "):
                author = line.replace("author: ", "").replace("\n", "")
                if author not in authors:
                    authors[author] = []
                authors[author].append(idea)
                if author not in contributors:
                    contributors[author] = []
                contributors[author].append(idea)
    if idea["category"] not in categories:
        categories[idea["category"]] = []
    categories[idea["category"]].append(idea)

with open(README, "r") as f:
    pretext = f.read()

with open(README, "w") as f:
    TEXT = "\n"
    for category in categories:
        TEXT += f"- [{category}](#{category.lower()})\n"

    for category in categories:
        TEXT += f"### {category}\n"
        for idea in categories[category]:
            title = idea["title"]
            file = idea["file"]
            short = idea["short"]
            TEXT += f"- [**{title}**]({file}) - [{short}]({file})\n"

    f.write(
        pretext.replace(
            pretext.split("<!-- ideas.start -->")[1].split("<!-- ideas.end -->")[0],
            TEXT,
            1,
        )
    )

with open(CONTRIBUTORS, "r") as f:
    pretext = f.read()

with open(CONTRIBUTORS, "w") as f:
    TEXT = "\n"
    for contributor in contributors:
        TEXT += f"### {contributor}\n"
        for idea in contributors[contributor]:
            title = idea["title"]
            file = idea["file"]
            short = idea["short"]
            TEXT += f"- [**{title}**]({file}) - [{short}]({file})\n"

    f.write(
        pretext.replace(
            pretext.split("<!-- contributors.start -->")[1].split(
                "<!-- contributors.end -->"
            )[0],
            TEXT,
            1,
        )
    )

with open(CODEOWNERS, "r") as f:
    pretext = f.read()

with open(CODEOWNERS, "w") as f:
    TEXT = "\n"
    for author in authors:
        for idea in authors[author]:
            title = idea["title"]
            file = idea["file"]
            short = idea["short"]
            TEXT += f"{file} @{author}\n"

    f.write(
        pretext.replace(
            pretext.split("#authors.start")[1].split("#authors.end")[0], TEXT, 1
        )
    )
